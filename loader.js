var path = require('path');

var loadedPlugins = {};

var loadDependencies = function(config, baseDir, dependencies) {
	for (var i = 0; i < dependencies.length; ++i)
		loadPlugin(config, baseDir, dependencies[i]);
};

var loadPlugin = function(config, baseDir, name) {
	if (loadedPlugins[name] === undefined) {
		var doLoad = true;
		for (var e in config.plugins[name]) doLoad = (e == name ? false : true);
		if (doLoad === true && config.plugins[name] !== undefined)
			loadDependencies(config, baseDir, config.plugins[name]);
		var pluginPath = path.resolve(__dirname, './' + baseDir, name);
		require(pluginPath).run();
		loadedPlugins[name] = true;
	}
};

exports.plugin = loadPlugin;