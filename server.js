var express = require('express');
var argv = require('optimist').argv;
var path = require('path');
var loader = require('./loader');

var app = express();

try {
	var configName = argv.c || 'default';
	var configPath = path.resolve(__dirname, "./configs/", configName);
	var config = require(configPath);
} catch (err) {
	console.log(err);
	process.exit(1);
}

function boot() {
	if (config.debug) {
		app.use(function(req, res, next){
		  console.log('%s %s', req.method, req.url);
		  next();
		});
	}

	app.use(express.static(__dirname + '/public'));

	for (var name in config.plugins)
		loader.plugin(config, 'plugins', name);

	app.listen(3000);
}

try {
	boot();
} catch (err) {
	if (config.debug)
		console.log(err.stack);
	process.exit(1);
}

