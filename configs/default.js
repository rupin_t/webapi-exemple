"use strict"

var argv = require('optimist').argv;

var port = argv.p || process.env.PORT || 3000;
var host = argv.l || process.env.IP || 'localhost';

var config = {
	debug: argv.d || false,
	port: port,
	host: host,
	plugins: { // plugin name + dependencies
		core: []
	}
};

module.exports = config;